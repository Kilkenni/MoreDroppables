# More Droppables

## CONTENTS

mechbeacon - now drops itself as an item when broken. Now printable.
toxicwastebarrel - now drops itself as an item when broken.
explosivebarrel - now drops itself as an item when broken.
lasertripwire - now drops itself as an item when broken. Now printable.
scorchedlandmine - now drops itself as an item when broken.  Triggered when smashed.
landmine - now drops itself as an item when broken. Triggered when smashed.
metallictrapdoor - now drops itself as an item when broken.

## TODO:
lasertripwire - Trigger when you break it with MM.

##CONFLICTS

Enhanced Storage - probably (changed some barrels into storage items).
Other mods directly changing the same objects.
Mods changing the "/objects/wired/landmine/landmine.lua" script

## THANKS
C0bra5 - for his useful Recolor Tool (http://community.playstarbound.com/threads/update-v1-1-2-recolor-maker-2.105981/)

It would take me ages to create palettes without it.

C0rdyc3p5 - for his friendly SMK, the StarModder's Kit (http://community.playstarbound.com/threads/starmodder-kit-the-modding-ide-wip.117686/)

I usually use it for packing/unpacking my .pak files.
